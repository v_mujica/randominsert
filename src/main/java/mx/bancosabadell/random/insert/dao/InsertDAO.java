package mx.bancosabadell.random.insert.dao;

import mx.bancosabadell.random.insert.entity.SpOrdPagoRec;
import org.springframework.data.repository.CrudRepository;

public interface InsertDAO extends CrudRepository<SpOrdPagoRec, Integer> {

}