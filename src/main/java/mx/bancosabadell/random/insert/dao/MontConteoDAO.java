package mx.bancosabadell.random.insert.dao;

import org.springframework.data.repository.CrudRepository;

import mx.bancosabadell.random.insert.entity.MontConteo;

public interface MontConteoDAO extends CrudRepository<MontConteo, Integer> {

}
