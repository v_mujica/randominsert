package mx.bancosabadell.random.insert.dao;

import org.springframework.data.repository.CrudRepository;

import mx.bancosabadell.random.insert.entity.MontDetalle;

public interface MontDetalleDAO extends CrudRepository<MontDetalle, Integer> {

}
