package mx.bancosabadell.random.insert;

import lombok.extern.java.Log;
import mx.bancosabadell.random.insert.service.InsertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@Log
@SpringBootApplication
public class RandomInsertApplication implements CommandLineRunner {

    @Autowired
    private InsertService insertService;

    public static void main(String[] args) {
        SpringApplication.run(RandomInsertApplication.class);
    }

    @Override
    public void run(String... args) throws Exception {
        insertService.insertarElementosMonitoreo();
    }

    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("Mexico/General"));
        log.info("Sistema Iniciado");
    }
}