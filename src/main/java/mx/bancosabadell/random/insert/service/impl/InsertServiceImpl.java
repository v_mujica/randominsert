package mx.bancosabadell.random.insert.service.impl;

import mx.bancosabadell.random.insert.dao.InsertDAO;
import mx.bancosabadell.random.insert.dao.MontConteoDAO;
import mx.bancosabadell.random.insert.dao.MontDetalleDAO;
import mx.bancosabadell.random.insert.entity.MontConteo;
import mx.bancosabadell.random.insert.entity.MontDetalle;
import mx.bancosabadell.random.insert.entity.SpOrdPagoRec;
import mx.bancosabadell.random.insert.service.InsertService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Random;

@Service
public class InsertServiceImpl implements InsertService {

    private static final Logger LOGGER = LogManager.getLogger(InsertServiceImpl.class);
    private static final String GRR = "GERARDO RAYAS REYES";
    @Autowired
    private InsertDAO insertDAO;
    @Autowired
    private MontConteoDAO montConteoDAO;
    @Autowired
    private MontDetalleDAO montDetalleDAO;
    @Autowired
    private Random random;

    private static int getIntToday() {
        final String pattern = "yyyyMMdd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date());
        return Integer.parseInt(date);
    }

    private static long getEpochTimeStamp() {
        LocalDateTime ahora = LocalDateTime.now().plusHours(6);
        return ahora.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
    }

    @Override
    public void insertarElementosSpOrdPagoRec() {
        int id = 700012;
        boolean flag = true;
        while (flag) {
            insertElementosSpOrdPagoRec(id++);
            try {
                Thread.sleep(4000);
            } catch (InterruptedException e) {
                flag = false;
                LOGGER.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    @Transactional
    private void insertElementosSpOrdPagoRec(Integer id) {
        SpOrdPagoRec obj = null;

        obj = new SpOrdPagoRec();

        obj.setId(id);
        obj.setCde("e26432c56403d64a56512352c4106d1e43c7dc19762d47eb");
        obj.setClaveRastreo("2020010740000AAAAA0000000000000");
        obj.setConceptoPago("TEST GERARDO RAYAS");
        obj.setCuentaBeneficiario("5580808080");
        obj.setCuentaOrdenante("010101010101010101");
        obj.setFechaCaptura(getIntToday());
        obj.setFechaLiquidacion(getIntToday());
        obj.setFolioOrden(2);
        obj.setFolioPaquete(2222);
        obj.setIva(0);
        obj.setMonto(getMontosRandom());
        obj.setNombreBeneficiario(GRR);
        obj.setNombreOrdenante(GRR);
        obj.setPrioridad(0);
        obj.setReferenciaCobranza("0");
        obj.setReferenciaNumerica(1234567);
        obj.setRfcCurpBeneficiario("ND");
        obj.setRfcCurpOrdenante("RARG880101ABC");
        obj.setTopologia("V");
        obj.setTsCaptura(getEpochTimeStamp());
        obj.setTsEntrega(1234);
        obj.setTsLiquidacion(3133);
        obj.setVersion(5);
        obj.setFechaOperacion(getIntToday());
        obj.setFolioOrdenCep(1);
        obj.setFolioPaqueteCep(1);
        obj.setNombreClienteCep(GRR);
        obj.setRfcCep("RARG880101ABCABC01");
        obj.setTsConfirmacion(getIntToday());
        obj.setEmpresa("SABADELL");
        obj.setError(0);
        obj.setEstado(getEstadosRandom());
        obj.setInsContraparte(40014);
        obj.setInsOperante(40156);
        obj.setMedioEntrega(1);
        obj.setTipoCuentaBeneficiario(10);
        obj.setTipoCuentaOrdenante(40);
        obj.setTipoPago(1);
        obj.setTsAcuseConfirmacion(getIntToday());
        obj.setTipoOperacion(4);
        obj.setCausaDevolucion(null);
        obj.setPoa(0);

        insertDAO.save(obj);
    }

    @Override
    public void insertarElementosMonitoreo() {
        boolean flag = true;
        while (flag) {
            insertElementosMonitoreo();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                flag = false;
                LOGGER.error(e.getMessage());
                Thread.currentThread().interrupt();
            }
        }
    }

    @Transactional
    private void insertElementosMonitoreo() {
        final int min = 0;
        final int max = 1000;
        final double minMonto = 0.0;
        final double maxMonto = 1000.0;
        Date fecha = new Date();
        String estado = getEstadosRandom();
        MontConteo objConteo = new MontConteo();
        objConteo.setFecha(fecha);
        objConteo.setEstado(estado);
        objConteo.setDescripcion("TEST");
        objConteo.setCantidad(random.nextInt(max - min + 1) + min);
        objConteo.setError(0);
        montConteoDAO.save(objConteo);

        MontDetalle objDetalle = new MontDetalle();
        objDetalle.setEstado(estado);
        objDetalle.setError(0);
        objDetalle.setFecha(fecha);
        objDetalle.setRazonRechazo("");
        objDetalle.setMonto(minMonto + (maxMonto - minMonto) * random.nextDouble());
        montDetalleDAO.save(objDetalle);
    }

    private String getEstadosRandom() {
        String[] estados = {"A", "C", "CA", "CCE", "CCO", "CCR", "CL", "CN", "CR", "CXO", "D", "E", "EA", "EC", "EL",
                "EP", "L", "LQ", "PA", "PL", "R", "RA", "RB", "RE", "RL", "TA", "TC", "TCL", "TL", "TLQ", "XC", "XD",
                "XR"};
        int posicion = (random.nextInt() * 37) % estados.length;
        return estados[posicion];
    }

    private int getMontosRandom() {
        final int min = 100;
        final int max = 10000;
        return random.nextInt(max + 1) + min;
    }
}
