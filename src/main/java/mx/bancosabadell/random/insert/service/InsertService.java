package mx.bancosabadell.random.insert.service;

public interface InsertService {
    void insertarElementosSpOrdPagoRec();

    void insertarElementosMonitoreo();
}
