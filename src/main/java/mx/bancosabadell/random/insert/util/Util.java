package mx.bancosabadell.random.insert.util;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

@Component
public class Util {
    private Util() {
    }

    @Bean
    public static Random getRandom() throws NoSuchAlgorithmException {
        return SecureRandom.getInstanceStrong();

    }
}
