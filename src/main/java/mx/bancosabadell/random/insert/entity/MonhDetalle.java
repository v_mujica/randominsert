package mx.bancosabadell.random.insert.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity(name = "monh_detalle")
public class MonhDetalle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Integer id;

    @Column(name = "fecha")
    private Timestamp fecha;

    @Column(name = "error")
    private String error;

    @Column(name = "razon_rechazo")
    private String razonRechazo;

    @Column(name = "monto")
    private Double monto;
}