package mx.bancosabadell.random.insert.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "sp_ord_pago_rec")
public class SpOrdPagoRec {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "cde")
    private String cde;

    @Column(name = "clave_pago")
    private String clavePago;

    @Column(name = "clave_rastreo")
    private String claveRastreo;

    @Column(name = "clave_rastreo_dev")
    private String claveRastreoDev;

    @Column(name = "concepto_pago")
    private String conceptoPago;

    @Column(name = "concepto_pago2")
    private String conceptoPago2;

    @Column(name = "cuenta_beneficiario")
    private String cuentaBeneficiario;

    @Column(name = "cuenta_beneficiario2")
    private String cuentaBeneficiario2;

    @Column(name = "cuenta_ordenante")
    private String cuentaOrdenante;

    @Column(name = "detalle_error")
    private String detalleError;

    @Column(name = "fecha_captura")
    private Integer fechaCaptura;

    @Column(name = "fecha_liquidacion")
    private Integer fechaLiquidacion;

    @Column(name = "folio_banxico")
    private Integer folioBanxico;

    @Column(name = "folio_orden")
    private Integer folioOrden;

    @Column(name = "folio_paquete")
    private Integer folioPaquete;

    @Column(name = "iva")
    private Integer iva;

    @Column(name = "monto")
    private Integer monto;

    @Column(name = "nombre_beneficiario")
    private String nombreBeneficiario;

    @Column(name = "nombre_beneficiario2")
    private String nombreBeneficiario2;

    @Column(name = "nombre_ordenante")
    private String nombreOrdenante;

    @Column(name = "prioridad")
    private Integer prioridad;

    @Column(name = "razon_rechazo")
    private String razonRechazo;

    @Column(name = "referencia_cobranza")
    private String referenciaCobranza;

    @Column(name = "referencia_numerica")
    private Integer referenciaNumerica;

    @Column(name = "rfc_curp_beneficiario")
    private String rfcCurpBeneficiario;

    @Column(name = "rfc_curp_beneficiario2")
    private String rfcCurpBeneficiario2;

    @Column(name = "rfc_curp_ordenante")
    private String rfcCurpOrdenante;

    @Column(name = "topologia")
    private String topologia;

    @Column(name = "ts_captura")
    private Long tsCaptura;

    @Column(name = "ts_devolucion")
    private Integer tsDevolucion;

    @Column(name = "ts_entrega")
    private Integer tsEntrega;

    @Column(name = "ts_liquidacion")
    private Integer tsLiquidacion;

    @Column(name = "version")
    private Integer version;

    @Column(name = "cadena_original_cep")
    private String cadenaOriginalCep;

    @Column(name = "fecha_operacion")
    private Integer fechaOperacion;

    @Column(name = "folio_orden_cep")
    private Integer folioOrdenCep;

    @Column(name = "folio_paquete_cep")
    private Integer folioPaqueteCep;

    @Column(name = "nombre_cliente_cep")
    private String nombreClienteCep;

    @Column(name = "rfc_cep")
    private String rfcCep;

    @Column(name = "sello_cep")
    private String selloCep;

    @Column(name = "ts_confirmacion")
    private Integer tsConfirmacion;

    @Column(name = "causa_devolucion")
    private Integer causaDevolucion;

    @Column(name = "cuenta_liquidadora")
    private String cuentaLiquidadora;

    @Column(name = "empresa")
    private String empresa;

    @Column(name = "error")
    private Integer error;

    @Column(name = "estado")
    private String estado;

    @Column(name = "ins_contraparte")
    private Integer insContraparte;

    @Column(name = "ins_operante")
    private Integer insOperante;

    @Column(name = "medio_entrega")
    private Integer medioEntrega;

    @Column(name = "tipo_cuenta_beneficiario")
    private Integer tipoCuentaBeneficiario;

    @Column(name = "tipo_cuenta_beneficiario2")
    private Integer tipoCuentaBeneficiario2;

    @Column(name = "tipo_cuenta_ordenante")
    private Integer tipoCuentaOrdenante;

    @Column(name = "tipo_operacion")
    private Integer tipoOperacion;

    @Column(name = "tipo_pago")
    private Integer tipoPago;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "usuario_autoriza")
    private String usuarioAutoriza;

    @Column(name = "error_cep")
    private Integer errorCep;

    @Column(name = "ts_acuse_confirmacion")
    private Integer tsAcuseConfirmacion;

    @Column(name = "poa")
    private Integer poa;

    @Column(name = "folio_ord_original")
    private Integer folioOrdOriginal;

    @Column(name = "folio_paq_original")
    private Integer folioPaqOriginal;

    @Column(name = "fecha_op_original")
    private Integer fechaOpOriginal;

    @Column(name = "monto_interes")
    private Integer montoInteres;

    @Column(name = "monto_original")
    private Integer montoOriginal;

    @Column(name = "digito_id_ordenante")
    private Integer digitoIdOrdenante;

    @Column(name = "digito_id_beneficiario")
    private Integer digitoIdBeneficiario;

    @Column(name = "num_celular_ordenante")
    private String numCelularOrdenante;

    @Column(name = "num_celular_beneficiario")
    private String numCelularBeneficiario;

    @Column(name = "fecha_limite_pago")
    private String fechaLimitePago;

    @Column(name = "indicador_beneficiario")
    private Integer indicadorBeneficiario;

    @Column(name = "folio_plataforma")
    private String folioPlataforma;

    @Column(name = "pago_comision")
    private Integer pagoComision;

    @Column(name = "monto_comision")
    private Integer montoComision;

    @Column(name = "uetr_swift")
    private String uetrSwift;

    @Column(name = "campo_swift1")
    private String campoSwift1;

    @Column(name = "campo_swift2")
    private String campoSwift2;

    @Column(name = "folio_cobro_spei")
    private String folioCobroSpei;

    @Column(name = "serie_crt")
    private String serieCrt;
}
