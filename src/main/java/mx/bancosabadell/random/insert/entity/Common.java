package mx.bancosabadell.random.insert.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
class Common {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;

    @Column(name = "estado")
    private String estado;

    @Column(name = "error")
    private Integer error;
}
