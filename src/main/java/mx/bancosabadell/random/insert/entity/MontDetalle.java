package mx.bancosabadell.random.insert.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity(name = "mont_detalle")
public class MontDetalle extends Common {

    @Column(name = "razon_rechazo")
    private String razonRechazo;

    @Column(name = "monto")
    private Double monto;
}
